CREATE DATABASE IF NOT EXISTS `flaskdb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `flaskdb`;

DROP TABLE IF EXISTS `positions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `record_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `position` json NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
