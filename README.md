# Flask + gEvent + SQLAlchemy + mySQL - Concurrency and Performance

To run the flask app, first create a virtual environment and install everything.
Make sure you are using Python 3
```
virtualenv flask_mysql_env
source flask_mysql_env/bin/activate
pip install -r requirements.txt
```

To install the driver of mysqlclient (MysqlDb):
```
sudo apt-get install python3-dev  # --> For python 3.5 or lower
sudo apt-get install python3.6-dev  # --> For python 3.6 or higher
sudo apt-get install libmysqlclient-dev
```

Now run the docker with the mysql server:
```
sudo docker run --name flask_db -v ~/flask_mysql_gevent/db:/docker-entrypoint-initdb.d -p 3306:3306 -e MYSQL_ROOT_PASSWORD=qwer1234 -d mysql
```

To run the performance test with PyMysql driver:
```
MYSQL_DRIVER=PYMYSQL gunicorn --log-level info --bind 0.0.0.0:5000 --worker-class=gevent flask_mysql.main:app
```

To run the performance test with MysqlDB driver:
```
MYSQL_DRIVER=MYSQLCLIENT gunicorn --log-level info --bind 0.0.0.0:5000 --worker-class=gevent flask_mysql.main:app
```

To run the performance test - 1000 requests, with a concurrency of 2 (2 requests in parallel at a time):
```
apt-get install apache2-utils
ab -n 1000 -c 3 http://localhost:5000/make_test
```

Results with PyMysql:
```
Requests per second:    532.03 [#/sec] (mean)
```

Results with MysqlDB (using gEvent waiter, but *NOT* greenify, which could have improved the results significantly):
```
Requests per second:    168.73 [#/sec] (mean)
```
