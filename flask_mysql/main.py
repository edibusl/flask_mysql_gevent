import gevent
from gevent import monkey
monkey.patch_all()

import logging
import sys
import datetime
import os
import json
from flask import Flask, request, jsonify, Response, Blueprint
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, Column, Integer, JSON, DateTime
from sqlalchemy.ext.declarative import declarative_base

# SQL Alchemy "positions" test table declaration
Base = declarative_base()
class Position(Base):
    __tablename__ = 'positions'
    id = Column(Integer, primary_key=True, nullable=False)
    record_time = Column(DateTime)
    position = Column(JSON)


# Init needed modules
this = sys.modules[__name__]
this.logger = logging.getLogger(__name__)
this.some_index = 0
this.orm_engine = None
this.SessionFactory = None

# Init flask server
global app
app = Flask(__name__)


def init_logger():
    logging.basicConfig(level=logging.INFO)

def init_db(app):
    if os.environ['MYSQL_DRIVER'] == "PYMYSQL":
        this.orm_engine = create_engine('mysql+pymysql://root:qwer1234@127.0.0.1/flaskdb')
    else:
        this.orm_engine = create_engine('mysql+mysqldb://root:qwer1234@127.0.0.1/flaskdb',strategy='threadlocal', connect_args={'waiter': gevent_waiter})

    this.SessionFactory = sessionmaker(bind=this.orm_engine)


@app.route('/make_test', methods=['GET'])
def make_test():
    this.logger.info('Entering')

    # connection = db_manager.this.orm_engine.connect()
    # session = db_manager.this.SessionFactory(bind=connection)
    session = this.SessionFactory()
    pos = None

    try:
        this.some_index += 1
        cur_pos = {}
        cur_pos['lng'] = this.some_index
        cur_pos['lat'] = this.some_index

        pos = Position(position=json.dumps(cur_pos), record_time=datetime.datetime.now())
        session.add(pos)
        session.commit()
    except:
        this.logger.exception("Error while updating position.")
        raise
    finally:
        session.close()

    this.logger.info('Exiting')

    return jsonify({"res" : "OK"})

# Move control to gevent before a DB query is executed
def gevent_waiter(fd, hub=gevent.hub.get_hub()):
    hub.wait(hub.loop.io(fd, 1))


# Init logger and DB on first time
init_logger()
init_db(app)
